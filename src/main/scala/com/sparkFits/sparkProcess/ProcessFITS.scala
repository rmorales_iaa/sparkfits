/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  13/Oct/2023
 * Time:  13h:22m
 * Description: Process FITS in Spark
 *
 * run at least once:
 *  dfsRestart   : start hadoop
 *  sparkMasterRestart : start spark master
 *  sparkWorkerRestart : in each worker node
 *
 * Check that hadoop is alive in: http://nakbe:9870/dfshealth.html#tab-datanode
 * Check that spark master is on and working nodes running at:  http://nakbe:8080/
 * Be sure to use sbt compiler (not the bundled one) in case of use the IDE Intellj Ideea:
 *  Settings -> Build, Execution Decode -> sbt -> launcher -> custom
 */
//=============================================================================
package com.sparkFits.sparkProcess
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.pixelDataType.PixelDataType
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.standard.fits.{Fits, FitsLoad}
import com.common.geometry.point.Point2D
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.common.stream.remote.RemoteInputStream
import com.common.util.file.MyFile
import com.common.util.time.Time
import com.common.util.util.Util
//=============================================================================
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.sql.Dataset
import org.mongodb.scala.bson.collection.immutable.Document

import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object ProcessFITS extends Serializable with MyLogger {
  //---------------------------------------------------------------------------
  private var logDatabase : LogDatabase = null
  private var resultDatabase : ResultDatabase = null
  //---------------------------------------------------------------------------
  def loadHDFS_Fits(hdfs: FileSystem
                    , filePath: String
                    , readDataBlockFlag: Boolean = true): Option[Fits] = {
    var fits: Fits = null
    Try {
      info(s"Loading FITS file from hadoop:'$filePath'")
      val stream = RemoteInputStream(hdfs, filePath.trim)
      info(s"Parsing FITS:'$filePath'")
      fits = FitsLoad.build(stream,readDataBlockFlag).getOrElse(return None)
      if (fits.isMEF()) {
        error(s"Can not process multi-extension FITS in file: '$filePath'");
        return None
      }
    }
    match {
      case Success(_) =>  Some(fits)
      case Failure(e: Exception) =>
        exception(e, s"Error processing '$filePath'")
        None
    }
  }
  //---------------------------------------------------------------------------
  def buildImageInMemory(fs: FileSystem
                         , filePath: String
                         , readDataBlockFlag: Boolean = true): Option[MyImage] = {
    var img: Option[MyImage] = None

    Try {
      info(s"Loading the file from HDFS: '$filePath'")
      val fits = loadHDFS_Fits(fs, filePath, readDataBlockFlag).getOrElse(return None)

      //build the image to be processed
      info(s"Building FITS blocks")
      val primaryHDU = fits.getPrimaryHdu()
      val pixelByteSeq = primaryHDU.dataBlockSeq.flatMap { _.byteSeq }
      val recordMap = fits.getPrimaryHdu().recordMap
      val naxisSeq = recordMap.getAxisSeq()
      if (naxisSeq.length != 2) {
        addToLogDatabase(s"Only can process images with 2 axis in file: '$filePath'")
        error(s"Only can process images with 2 axis in file: '$filePath'");
        return None
      }
      val bitPix = recordMap.getBitPix().toInt
      val bScale = recordMap.getBscale()
      val bZero = recordMap.getBzero()
      val expectedByteSize = (naxisSeq.head * naxisSeq.last) * Math.abs(bitPix / 8)
      val pixelSeq = PixelDataType.getPixelSeq(pixelByteSeq.take(expectedByteSize.toInt), bitPix)
      val scaledPixSeq = PixelDataType.scalePixelSeq(pixelSeq, bScale, bZero)

      //rebuild the header in the proper order
      val fitsHeader = SimpleFits()
      recordMap.toPlainMap().foreach { case (key, value) =>
        fitsHeader.updateRecord(key, value)
      }
      fitsHeader.bitPix = bitPix.toByte
      fitsHeader.bScale = bScale
      fitsHeader.bZero  = bZero
      fitsHeader.width  = naxisSeq.head
      fitsHeader.height = naxisSeq.last

      info(s"Building image")
      img = Some(new MyImage(naxisSeq.head.toInt
                             , naxisSeq.last.toInt
                             , scaledPixSeq
                             , Point2D.POINT_ZERO
                             , name = filePath
                             , userFits = Some(fitsHeader))
      )
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error processing: '$filePath'")
    }
    img
  }
  //---------------------------------------------------------------------------
  def getSubImageSeq(spark:MySpark
                     , filePath: String
                     , subWidth: Int
                     , subHeight: Int): (Array[SubImage], Point2D) = {
    val img = buildImageInMemory(spark.myHadoop.get.fs, filePath).getOrElse {
      error(s"Error loading FITS file: '$filePath'")
      return (Array(),Point2D.POINT_ZERO)
    }
    val fits = img.getSimpleFits()
    val imageWidth  = fits.width.toInt
    val imageHeight =  fits.height.toInt

    info(s"Input image: ${imageWidth}x$imageHeight pixels. Creating the a sequence of sub-images ${subWidth}x$subHeight pixels")

    val axisPartitionSeq = Point2D.getAxisPartitionSeq(
      imageWidth
      , imageHeight
      , subWidth
      , subHeight)

    info(s"Creating ${axisPartitionSeq.length} sub-images of ${subWidth}x$subHeight pixels")
    val subImageSeq = {
      axisPartitionSeq map { case (minPos, maxPos) =>
        SubImage(img
                 , minPos
                 , maxPos)
      }
    }
    (subImageSeq,Point2D(imageWidth,imageHeight))
  }

  //-----------------------------------------------------------------------------
  def dropDatabaseSeq() = {
    Try {
      val db = ResultDatabase(collectionName = "none")
      db.dropDatabase()
      db.close()

      val dbLog = LogDatabase(collectionName = "none")
      dbLog.dropDatabase()
      dbLog.close()
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error dropping the databases")
    }
  }

  //-----------------------------------------------------------------------------
  def closeDatabaseSeq(): Unit = {
    Try {
      if (logDatabase != null) logDatabase.close()
      if (resultDatabase != null) resultDatabase.close()
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error closing the databases")
    }
  }

  //-----------------------------------------------------------------------------
  def addResultToDatabase(filePath: String
                          , sourceCount: Long
                          , startMs: Long) = {

    val elapsedTime = Time.getFormattedElapseTimeFromStart(startMs)
    val pathSeq = filePath.split("/")
    val fileName = MyFile.removeFileExtension(pathSeq.last)
    val collectionName = pathSeq(pathSeq.length - 2)
    val entryValue = s"{${Util.getComputerName},$sourceCount,$elapsedTime}"
    val doc = Document(
      "_id" -> fileName
      , "entry" -> entryValue)

    Try {
     if (resultDatabase == null) resultDatabase = ResultDatabase(collectionName = collectionName)
      resultDatabase.insert(doc)
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error adding to result database '$filePath'")
    }
  }
  //-----------------------------------------------------------------------------
  def addResultToDatabase(collectionName: String
                          , subImageID: String
                          , serializedSourceSeq: Array[String]
                          , startMs: Long) = {


    val doc = Document(
         "_id"         -> subImageID
       , "node"        -> Util.getComputerName
       , "elapsedTime" -> Time.getFormattedElapseTimeFromStart(startMs)
       , "sourceSeq"   -> serializedSourceSeq.toSeq
    )

    Try {
      if (resultDatabase == null) resultDatabase = ResultDatabase(collectionName = collectionName)
      resultDatabase.insert(doc)
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error adding to result database '$collectionName'")
    }
  }
  //-----------------------------------------------------------------------------
  def addToLogDatabase(message: String) = {
    val doc = Document(
      "time_stamp" -> Time.getISO_DateLocalTimeStamp()
      , "node" -> s"${Util.getComputerName}"
      , "message" -> message
    )
    Try {
      if (logDatabase == null) logDatabase = LogDatabase()
      logDatabase.insert(doc)
    }
    match {
      case Success(_) =>
      case Failure(e: Exception) =>
        exception(e, s"Error adding to log database '$message'")
    }
  }
  //-----------------------------------------------------------------------------
  def getSubImageDataset(spark: MySpark
                         , filePath: String
                         , subWidth: Int
                         , subHeight: Int) = {
    //build the image dataset (but only the sub-image definition, not loading the pixels)
    val (subImageSeq,imageDimensions) = ProcessFITS.getSubImageSeq(spark, filePath, subWidth, subHeight)
    info(s"Created a data set with '${subImageSeq.length}' subImages")

    import spark.sparkSession.implicits._
    val ds = getSubImageDatasetRepartitioned(subImageSeq.toSeq.toDS())
    warning(s"Processing dataset with partition count: '${ds.rdd.getNumPartitions}'")
    (ds,imageDimensions)
  }
  //-----------------------------------------------------------------------------
  private def getSubImageDatasetRepartitioned(ds: Dataset[SubImage]): Dataset[SubImage] = {

    val sparkConf = MyConf(MyConf.c.getString("Spark.conf"))
    val partitionCount = sparkConf.getIntWithDefaultValue("Spark.computationCluster.remote.partitionCount", -1)

    val repartitionedDS =
      if (partitionCount == -1) {
        info("SPARK will decide the number of partitions")
        ds
      }
      else {
        info(s"Requested user repartition: '$partitionCount'")
        ds.repartition(partitionCount)
      }
    repartitionedDS
  }
  //---------------------------------------------------------------------------
  def run(spark: MySpark
          , inputPath: String
          , subWidth: Int
          , subHeight:Int
          , sigmaMultiplier: Double
          , sourceDetectionLevel: Double
          , sourceRestriction: Option[(Int,Int)]
          , limitDataSetItemCount: Option[Int]
          , memoryFlag: Boolean
          , memoryNsaFlag: Boolean
         ): Boolean = {

    val verbose = false
    var isDirectory = false
    var isFile = false

    val hadoop = spark.myHadoop.get
    if (hadoop.dirExist(inputPath)) isDirectory = true
    if (hadoop.fileExist(inputPath)) isFile = true

    if (isDirectory)
      CountSourcesInDirectory.run(spark
                                  , inputPath
                                  , subWidth
                                  , subHeight
                                  , sigmaMultiplier
                                  , limitDataSetItemCount)
    else CountSourcesInFile.run(spark
                                , inputPath
                                , subWidth
                                , subHeight
                                , sourceDetectionLevel
                                , sourceRestriction
                                , memoryFlag
                                , memoryNsaFlag
                                , verbose)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file ProcessFITS.scala
//=============================================================================