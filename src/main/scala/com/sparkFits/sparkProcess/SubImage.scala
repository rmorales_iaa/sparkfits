/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  14/Nov/2023
 * Time:  18h:54m
 * Description: None
 */
package com.sparkFits.sparkProcess
//=============================================================================
import com.common.fits.simpleFits.SimpleFits
import com.common.fits.standard.Keyword._
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.myImage.MyImage
import com.common.image.region.Region
import com.common.logger.MyLogger
import com.common.util.path.Path
//=============================================================================
//=============================================================================
object SubImage extends MyLogger {
  //---------------------------------------------------------------------------
  private final val SAVE_SUB_IMAGE_OUTPUT_PATH = "output/debugging/sub_image/"
  private final val SAVE_SUB_IMAGE_SOURCE_DETECTED_OUTPUT_PATH = "output/debugging/source_detected/"
  //---------------------------------------------------------------------------
  def apply(img: MyImage
            , minPos: Point2D
            , maxPos: Point2D): SubImage =
    SubImage(minPos
            , maxPos
            , img.getSubMatrix(minPos,maxPos).data)
  //---------------------------------------------------------------------------
}
//=============================================================================
import SubImage._
case class SubImage(minPos: Point2D
                    , maxPos: Point2D
                    , pixelSeq: Array[Double]) {
  //--------------------------------------------------------------------------
  def getID = minPos.toString  + "->" + maxPos.toString
  //--------------------------------------------------------------------------
  def getX_pixSize = maxPos.x - minPos.x + 1
  //--------------------------------------------------------------------------
  def getY_pixSize = maxPos.y - minPos.y + 1
  //--------------------------------------------------------------------------
  def getOffset = minPos
  //--------------------------------------------------------------------------
  def toMatrix2D = Matrix2D(getX_pixSize, getY_pixSize, pixelSeq)
  //--------------------------------------------------------------------------
  def toMatrix2D_WithOffset(o: Point2D) = Matrix2D(getX_pixSize, getY_pixSize, pixelSeq, offset = o)
  //--------------------------------------------------------------------------
  def contains(p:Point2D) =
    (p.x >= minPos.x  && p.x <= maxPos.x) &&
    (p.y >= minPos.y  && p.y <= maxPos.y)
  //--------------------------------------------------------------------------
  def save(subMatrix: Matrix2D
           , bitPix: Int
           , bScale: Double
           , bZero: Double
           , compiledSourceSeq: Array[Segment2D]) = {

    //build a FITS file from sub-matrix
    val simpleFits = SimpleFits()
    simpleFits.updateRecord(KEYWORD_SIMPLE, "T")
    simpleFits.updateRecord(KEYWORD_NAXIS, "2")
    simpleFits.updateRecord(KEYWORD_BITPIX, bitPix.toString)
    simpleFits.updateRecord(KEYWORD_BSCALE, bScale.toString)
    simpleFits.updateRecord(KEYWORD_BZERO, bZero.toString)
    simpleFits.updateRecord(KEYWORD_NAXIS + "1", getX_pixSize.toString)
    simpleFits.updateRecord(KEYWORD_NAXIS + "2", getY_pixSize.toString)
    simpleFits.updateRecord(KEYWORD_END, "")
    simpleFits.bitPix = bitPix.toByte
    simpleFits.bScale = bScale
    simpleFits.bZero = bZero

    Path.ensureDirectoryExist(SAVE_SUB_IMAGE_OUTPUT_PATH)
    MyImage(subMatrix, simpleFits)
      .saveAsFits(s"$SAVE_SUB_IMAGE_OUTPUT_PATH/${subMatrix.id}")

    //render the sources
    Path.ensureDirectoryExist(s"$SAVE_SUB_IMAGE_SOURCE_DETECTED_OUTPUT_PATH/${subMatrix.id}")
    val region = Region()
    region.addToStorage(compiledSourceSeq)
    region.synthesizeImageWithColor(s"$SAVE_SUB_IMAGE_SOURCE_DETECTED_OUTPUT_PATH/${subMatrix.id}.png"
      , imageDimension = Some(Point2D(getX_pixSize, getY_pixSize)))
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file SubImage.scala
//=============================================================================