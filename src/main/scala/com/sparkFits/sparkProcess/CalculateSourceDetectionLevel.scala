/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  12/May/2024
 * Time:  18h:14m
 * Description: None
 */
package com.sparkFits.sparkProcess
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.common.image.estimator.background.Simple.estimateBackgroundAndRms
import com.sparkFits.sparkProcess.ProcessFITS._
import com.common.dataType.conversion.DataTypeConversion._
import com.common.hadoop.inputFormat.binary.record.BinaryRecord._
import com.common.image.nsaImage.NSA_Image
import com.common.image.nsaImage.spark.NSA_ImageCalculateSourceDetectionLevel
import com.sparkFits.sparkProcess.hadoop.sourceDetectionLevel.BinaryFileFitsSrcDetLev
import org.apache.spark.storage.StorageLevel
//=============================================================================
import org.apache.hadoop.conf.Configuration
import scala.util.{Try,Success,Failure}
//=============================================================================
//=============================================================================
object CalculateSourceDetectionLevel extends MyLogger {
  //---------------------------------------------------------------------------
  private final val RECORD_BYTE_LENGTH = com.common.fits.standard.ItemSize.RECORD_CHAR_SIZE
  private final val splitByteLength = RECORD_BYTE_LENGTH * getSplitMultiplier
  //---------------------------------------------------------------------------
  private def getSplitMultiplier() = {
    var multiplier = 0
    Try {
      multiplier = MyConf(MyConf.c.getString("Spark.conf")).getInt("Hadoop.splitMultiplier")
    }
    match {
      case Success(_) =>
        multiplier  //running SPARK in local
      case Failure(_) =>
        1000 * 10  //default value: running SPARK in remote
    }
  }
  //---------------------------------------------------------------------------
  def processHaddopRecord(key: BinaryRecordKeyType
                          , value: BinaryRecordValueType): Option[(Double,Double)] = {
    if (value.getLength == 0) None
    else {
      val doubleSeq = byteSeqToDoubleSeq(value.getBytes)
      Some(doubleSeq.head, doubleSeq.last) //background and rms
    }
  }
  //---------------------------------------------------------------------------
  private def processSubImage(subImage: SubImage): (Double,Double) =
    estimateBackgroundAndRms(subImage.pixelSeq)
  //---------------------------------------------------------------------------
  def run(spark: MySpark
          , filePath: String
          , subWidth: Int
          , subHeight: Int
          , sigmaMultiplier: Double
          , memoryFlag: Boolean
          , memoryNsaFlag: Boolean
          , verbose: Boolean): Boolean = {

    //drop database results
    dropDatabaseSeq()

    //process the dataset
    val backGroundRmsSeq =
       if (memoryFlag) runInMemory(spark, filePath, subWidth, subHeight)
       else
          if (memoryNsaFlag)
            NSA_ImageCalculateSourceDetectionLevel.run(spark
                                                      , filePath
                                                      , subWidth
                                                      , subHeight)
          else runInHadoop(spark, filePath, verbose)

    //process the final result
    if (backGroundRmsSeq.isEmpty)
      error(s"None partial result calculated")
    else {
      val (estimatedBackground, estimatedBackgroundRMS) = NSA_Image.getBackgroundAndRMS(backGroundRmsSeq)
      val sourceDetectionLevel = estimatedBackground + (estimatedBackgroundRMS * sigmaMultiplier)
      info(s"Estimated source detection level: '$sourceDetectionLevel'")
    }

    //close databases
    closeDatabaseSeq()
    true
  }
  //---------------------------------------------------------------------------
  private def runInMemory(spark: MySpark
                          , filePath: String
                          , subWidth: Int
                          , subHeight: Int): Array[(Double,Double)] = {

    import spark.sparkSession.implicits._

    //get the dataset to be processed
    val (dataset, _) = getSubImageDataset(
        spark
      , filePath
      , subWidth
      , subHeight)

    //process the dataset
     dataset
       .persist(StorageLevel.MEMORY_AND_DISK)
       .mapPartitions { partition =>
        partition.map { subImage => processSubImage(subImage) }
      }.collect()
  }
  //---------------------------------------------------------------------------
  private def runInHadoop(spark: MySpark
                          , filePath: String
                          , verbose: Boolean): Array[(Double,Double)] = {

    var partialResult = Array[(Double,Double)]()
    Try {
      val conf = new Configuration(spark.myHadoop.get.hadoopConf)
      conf.setInt("mapred.max.split.size", splitByteLength)
      val hadoopRDD = spark.sparkContext.newAPIHadoopFile(
        filePath
        , classOf[BinaryFileFitsSrcDetLev] //inputFormatClass
        , classOf[BinaryRecordKeyType] //keyClass
        , classOf[BinaryRecordValueType] //valueClass
        , conf)

      // Use a map transformation to create copies of the records
      val rdd = hadoopRDD.map { case (key, value) =>
        (new BinaryRecordKeyType(key.get()), new BinaryRecordValueType(value.copyBytes))
      }


      import spark.sparkSession.implicits._
      partialResult =  rdd
        .persist(StorageLevel.MEMORY_AND_DISK)
        .mapPartitions { partition=>
        partition.flatMap { case (key,value) =>
          processHaddopRecord(key, value) }
      }.collect()
    }
    match {
      case Success(_) =>
      case Failure(ex) =>
        error(s"Error processing in hadoop mode file: '$filePath' " + ex.toString)
    }
    partialResult
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SourceDetectionLevel.scala
//=============================================================================