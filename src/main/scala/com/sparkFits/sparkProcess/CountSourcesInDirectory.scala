/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/May/2024
 * Time:  15h:23m
 * Description: None
 */
package com.sparkFits.sparkProcess
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.myImage.MyImage
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.sparkFits.sparkProcess.ProcessFITS._
import org.apache.spark.sql.Dataset
import org.apache.spark.storage.StorageLevel
//=============================================================================
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.conf.Configuration
//=============================================================================
//=============================================================================
object CountSourcesInDirectory extends Serializable with MyLogger {
  //---------------------------------------------------------------------------
  private final val HDFS_CORE_SITE_XML = "/opt/hadoop/hadoop_latest/etc/hadoop/core-site.xml" //file must exist in all working nodes
  private final val HDFS_SITE_XML = "/opt/hadoop/hadoop_latest/etc/hadoop/hdfs-site.xml" //file must exist in all working nodes
  //---------------------------------------------------------------------------
  private def processImage(img: MyImage
                           , subWidth: Int
                           , subHeight: Int
                           , sigmaMultiplier: Double): Int = {
    val (background, backgroundRMS) = img.estimateGlobalBackgroundAndRMS(subWidth, subHeight)
    img.setBackground(background)
    img.setBackgroundRMS(backgroundRMS)
    val noiseTide = background + (backgroundRMS * sigmaMultiplier)
    val sourceSeq = img.findSource(noiseTide).toSegment2D_seq()
    info(img.getRawName() + "with noise tide:" + noiseTide + " has:" + sourceSeq.length + " sources")
    sourceSeq.length
  }
  //-----------------------------------------------------------------------------
  private def processFile(fs: FileSystem
                          , filePath: String
                          , subWidth: Int
                          , subHeight: Int
                          , sigmaMultiplier: Double): Int = {


    val startMs = System.currentTimeMillis

    val img = buildImageInMemory(fs,filePath).getOrElse{
      addToLogDatabase(s"Error processing: '$filePath'")
      return Int.MinValue
    }
    val sourceCount = processImage(img, subWidth, subHeight, sigmaMultiplier)

    //add result to database
    addResultToDatabase(filePath, sourceCount, startMs)
    addToLogDatabase(s"Processed file: '$filePath' source count detected: '$sourceCount'")

    //final result
    sourceCount
  }

  //-----------------------------------------------------------------------------
  private def getDatasetRepartitioned(ds: Dataset[String]): Dataset[String] = {

    val sparkConf = MyConf(MyConf.c.getString("Spark.conf"))
    val partitionCount = sparkConf.getIntWithDefaultValue("Spark.computationCluster.remote.partitionCount", -1)

    val repartitionedDS =
      if (partitionCount == -1) {
        info("SPARK will decide the number of partitions")
        ds
      }
      else {
        info(s"Requested user repartition: '$partitionCount'")
        ds.repartition(partitionCount)
      }

    warning(s"Processing dataset with partition count: '${repartitionedDS.rdd.getNumPartitions}'")
    repartitionedDS
  }

  //-----------------------------------------------------------------------------
  def run(spark: MySpark
          , inputPath: String
          , subWidth: Int
          , subHeight: Int
          , sigmaMultiplier: Double
          , limitDataSetItemCount:Option[Int] = None): Boolean = {

    //drop database results
    dropDatabaseSeq()

    //build the dataset
    val fitsFileNameDS = spark.getDatasetFromDir(
      inputPath
      , MyConf.c.getStringSeq("Common.fitsFileExtension")
      , limitDataSetItemCount
    )

    import spark.sparkSession.implicits._

    //calculate the dataset repartition if it is required
    val repartitionedDS = getDatasetRepartitioned(fitsFileNameDS)

    //process the dataset
    val sourceCountSeq = repartitionedDS
      .persist(StorageLevel.MEMORY_AND_DISK)
      .mapPartitions { partition =>
        val hadoopConf = new Configuration()
        hadoopConf.addResource(new org.apache.hadoop.fs.Path(HDFS_CORE_SITE_XML))
        hadoopConf.addResource(new org.apache.hadoop.fs.Path(HDFS_SITE_XML))
        val fs = FileSystem.get(hadoopConf)

        partition.map { filePath => processFile(fs, filePath, subWidth, subHeight, sigmaMultiplier) }

      }.collect()

    //close databases
    closeDatabaseSeq()

    warning(s"Adding partial results: ${sourceCountSeq.sum.toString}")
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file CountSourcesInDirectory.scala
//=============================================================================