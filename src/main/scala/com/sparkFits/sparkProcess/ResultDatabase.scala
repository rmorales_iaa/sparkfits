/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/May/2024
 * Time:  13h:01m
 * Description: None
 */
package com.sparkFits.sparkProcess
//=============================================================================
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{ClientSession, Document, MongoClient, MongoCollection, MongoDatabase, SingleObservable}
//=============================================================================
//=============================================================================
object ResultDatabase {
  //---------------------------------------------------------------------------
  //database is created in the workers, so provide a default configuration
  private final val DEFAULT_DATABASE_NAME = "spark_app_results"
  private final val URI                   = "mongodb://rafa:rafaMongoIAA@nakbe:27017/"
  private final val COMPRESSOR            = "zlib"
  private final val IDLE_TIME_SECONDS     = 20
  //---------------------------------------------------------------------------
  final val COL_NAME_NODE_NAME    = "node_name"
  final val COL_NAME_SOURCE_COUNT = "source_count"
  final val COL_NAME_ELAPSED_TIME = "elapsed_name"
  //---------------------------------------------------------------------------
}
//=============================================================================
import ResultDatabase._
//=============================================================================
case class ResultDatabase(databaseName:String = ResultDatabase.DEFAULT_DATABASE_NAME
                          , collectionName:String) extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
                  init(false
                       , Some(URI)
                       , Some(COMPRESSOR)
                       , Some(IDLE_TIME_SECONDS)
                  )
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file ResultDatabase.scala
//=============================================================================