/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  07/May/2024
 * Time:  15h:25m
 * Description: None
 */
package com.sparkFits.sparkProcess
//=============================================================================
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.sparkFits.sparkProcess.ProcessFITS._
import com.common.geometry.matrix.matrix2D.Matrix2DSourceDetectionByAxisPartition.{axisPartitionFindSourceSeq, mergeSourceSeq}
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.source.{Source, SourceSynthesization, SubImageSourceDetection}
import com.common.image.nsaImage.spark.NSA_ImageCalculateSourceCount
import com.common.util.file.MyFile
//=============================================================================
import org.apache.spark.storage.StorageLevel
import org.mongodb.scala.bson.BsonString
//=============================================================================
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object CountSourcesInFile extends MyLogger {
  //---------------------------------------------------------------------------
  private def loadSourceSeqFromDatabase(filePath: String) = {
    val pathSeq = filePath.split("/")
    val fileName = MyFile.removeFileExtension(pathSeq.last)

    //deserialize the sources stored in the database for the image
    val resultDatabase = ResultDatabase(collectionName = fileName)

    val sourceSeq = resultDatabase.getAllDocuments(projectionFieldSeq = Seq("sourceSeq")).map { doc =>
      doc("sourceSeq").asArray().getValues.toArray.map { obj =>
        val deserializedSource = obj.asInstanceOf[BsonString].getValue
        val stringSeq = deserializedSource.split("&")
        val source = Segment2D.deserialization(stringSeq.head)
        source.setOffset(source.offset)
        source.setExtraInfo(stringSeq.last.toInt)
        source
      }
    }.flatten
    info(s"Recovered: '${sourceSeq.length}'  partial and complete sources")
    sourceSeq.toArray
  }

   //---------------------------------------------------------------------------
  private def loadSourceSeqFromDatabaseNSA(filePath: String) = {
    val pathSeq = filePath.split("/")
    val fileName = MyFile.removeFileExtension(pathSeq.last)

    //deserialize the sources stored in the database for the image
    val resultDatabase = ResultDatabase(collectionName = fileName)

    val sourceSeq = resultDatabase.getAllDocuments(projectionFieldSeq = Seq("sourceSeq")).map { doc =>
      doc("sourceSeq").asArray().getValues.toArray.map { obj =>
        val deserializedSource = obj.asInstanceOf[BsonString].getValue
        val encodedDataType = deserializedSource.take(1)
        val stringSeq = deserializedSource
          .drop(2)  //avoid encoded type
          .split("&")
        val source = Source.deserialization(encodedDataType, stringSeq.head)
        source.setOffset(source.offset)
        source.setExtraInfo(stringSeq.last.toInt)
        source
      }
    }.flatten
    info(s"Recovered: '${sourceSeq.length}' partial and complete sources")
    sourceSeq.toArray
  }
  //---------------------------------------------------------------------------
  private def processSubImageInMemory(fileName: String
                                     , subImage: SubImage
                                     , sourceDetectionLevel: Double): Long = {

    val startMs = System.currentTimeMillis
    val compiledSourceSeq = ArrayBuffer[Segment2D]()
    axisPartitionFindSourceSeq(subImage.toMatrix2D_WithOffset(subImage.minPos)
                              , noiseTide = sourceDetectionLevel
                              , compiledSourceSeq = compiledSourceSeq)

    val compiledSourceCount = compiledSourceSeq.length
    if (compiledSourceCount > 0) {
      val serializedSourceSeq = compiledSourceSeq map (source => source.serialization() +
        s"&${subImage.getID}" +
        s"&${source.getExtraInfo}"
        )
      addResultToDatabase(
          fileName
        , subImage.getID
        , serializedSourceSeq.toArray
        , startMs)
    }
    compiledSourceCount
  }
  //---------------------------------------------------------------------------
  private def generateResult(filePath: String
                             , sourceSizeRestriction: Option[(Int, Int)]
                             , partialSourceCountSeq: Long
                             , imageDimensions: Point2D) = {

    info(s"Generated sources (partial and completed) count in the dataset:'$partialSourceCountSeq'")

    val compiledSourceSeq = loadSourceSeqFromDatabase(filePath)
    info(s"Loaded from database (partial and completed) source count:'${compiledSourceSeq.length}'")

    val mergedSourceSeq = mergeSourceSeq(compiledSourceSeq, sourceSizeRestriction)
    info(s"Merged source seq count : ${mergedSourceSeq.length}")

    Segment2D.saveAsCsv(mergedSourceSeq ,"output/run_memory_flag.csv")

    info(s"Building png output file")
    MyImage.synthesizeImage("output/run_memory_flag"
                             , mergedSourceSeq
                             , imageDimensions
                             , blackAndWhite = true)

    mergedSourceSeq.length
  }
  //---------------------------------------------------------------------------
  def generateResultNSA(filePath: String
                        , imageDimensions: Point2D
                        , subImagePixSize: Point2D
                        , sourceSizeRestriction: Option[(Int, Int)]
                        , partialSourceCountSeq: Long
                        , verbose: Boolean): Int = {

    info(s"Generated sources (partial and completed) count in the dataset:'$partialSourceCountSeq'")

    val compiledSourceSeq = loadSourceSeqFromDatabaseNSA(filePath)
    info(s"Loaded from database (partial and completed) source count:'${compiledSourceSeq.length}'")
    if (compiledSourceSeq.length == 0) return 0

    implicit val numericAny: Numeric[Any] = compiledSourceSeq.head.numeric.asInstanceOf[Numeric[Any]]

    val mergedSourceSeq = SubImageSourceDetection.mergeSourceSeq(
      compiledSourceSeq.asInstanceOf[Array[Source[Any]]]
      , imageDimensions
      , subImagePixSize
      , sourceSizeRestriction
      , verbose)

    info(s"Merged source seq count : ${mergedSourceSeq.length}")
    Source.saveAsCsv("output/run_memory_nsa_flag.csv", mergedSourceSeq)

    info(s"Building png output file")
    SourceSynthesization.run("output/run_memory_nsa_flag.png"
                             , mergedSourceSeq
                             , imageDimensions.x
                             , imageDimensions.y
                             , font = None
                             , blackAndWhite = true
                           )

    mergedSourceSeq.length
  }
  //---------------------------------------------------------------------------
  def run(spark: MySpark
          , filePath: String
          , subWidth: Int
          , subHeight: Int
          , sourceDetectionLevel: Double
          , sourceSizeRestriction: Option[(Int,Int)]
          , memoryFlag: Boolean
          , memoryNsaFlag: Boolean
          , verbose: Boolean = false): Boolean = {

   //drop database results
   dropDatabaseSeq()

   val (partialSourceCountSeq, imageDimensions) =
     if (memoryFlag)
        runInMemory(spark
                    , filePath
                    , subWidth
                    , subHeight
                    , sourceDetectionLevel
                    , verbose)
      else
        if (memoryNsaFlag) {
          NSA_ImageCalculateSourceCount.run(spark
                                            , filePath
                                            , subWidth
                                            , subHeight
                                            , sourceDetectionLevel)
        }
        else runInHadoop(spark
                         , filePath
                         , subWidth
                         , subHeight
                         , sourceDetectionLevel
                         , verbose)

    if (memoryNsaFlag)
      generateResultNSA(
        filePath
        , imageDimensions
        , subImagePixSize = Point2D(subWidth,subHeight)
        , sourceSizeRestriction
        , partialSourceCountSeq
        , verbose)
    else
      generateResult(filePath
                    , sourceSizeRestriction
                    , partialSourceCountSeq
                    , imageDimensions)

    //close databases
    closeDatabaseSeq()

    true
  }
  //---------------------------------------------------------------------------
  private def runInMemory(spark: MySpark
                          , filePath: String
                          , subWidth: Int
                          , subHeight: Int
                          , sourceDetectionLevel: Double
                          , verbose: Boolean): (Long,Point2D) = {

    //get the dataset to be processed
    val (dataset, imageDimensions) = getSubImageDataset(spark
                                                        , filePath
                                                        , subWidth
                                                        , subHeight)
    //process the dataset
    import spark.sparkSession.implicits._
    val pathSeq = filePath.split("/")
    val fileName = MyFile.removeFileExtension(pathSeq.last)
    val partialSourceCountSeq = dataset
      .persist(StorageLevel.MEMORY_AND_DISK)
      .mapPartitions { partition =>
        partition.map { subImage => processSubImageInMemory(fileName, subImage, sourceDetectionLevel) }
      }.collect()
      .sum
    (partialSourceCountSeq,imageDimensions)
  }
  //---------------------------------------------------------------------------
  private def runInHadoop(spark: MySpark
                          , filePath: String
                          , subWidth: Int
                          , subHeight: Int
                          , sourceDetectionLevel: Double
                          , verbose: Boolean): (Long,Point2D) = {

    error("Not implemented")
    (0,Point2D.POINT_ZERO)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
//End of file SourceDetectionCountFile.scala
//=============================================================================