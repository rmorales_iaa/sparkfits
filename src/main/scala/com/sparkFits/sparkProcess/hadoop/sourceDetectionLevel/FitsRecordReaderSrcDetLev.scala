/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Jun/2024
 * Time:  23h:23m
 * Description: None
 */
package com.sparkFits.sparkProcess.hadoop.sourceDetectionLevel
//=============================================================================
import com.common.dataType.conversion.DataTypeConversion._
import com.common.hadoop.fits.record.FitsRecordReader
import com.common.hadoop.inputFormat.binary.record.BinaryRecordValue
import com.common.image.estimator.background.Simple.estimateBackgroundAndRms
//=============================================================================
//=============================================================================
class FitsRecordReaderSrcDetLev(recordByteLength: Long) extends FitsRecordReader(recordByteLength){
  //---------------------------------------------------------------------------
  override def readValueFromStream(byteLength: Long): Boolean = {

    val streamReadPos = getAbsoluteStreamReadPos

    if ((streamReadPos + byteLength) < dataOffset) { //FITS header
      recordValue = new BinaryRecordValue(Seq[Byte]())
      recordValue.setSize(0)
      streamLongSkip(byteLength)  //go to next record
      return true
    }


    val (result, byteSeq) = streamLongRead(byteLength)
    if (!result) {
      errorMessage(s"Split $splitID. Error reading record " + getRelativeRecordIndex)
      return false
    }
    else {
      val droppedByteCount = dataOffset - streamReadPos
      val pixSeq =
        if (droppedByteCount > 0) getPixSeq(byteSeq.drop(droppedByteCount.toInt).toArray).getOrElse { return false }
        else getPixSeq(byteSeq.toArray).getOrElse { return false }

      val (backGround, rms) = estimateBackgroundAndRms(pixSeq)
      recordValue = new BinaryRecordValue(doubleSeqToByteSeq(Array(backGround, rms)))
      recordValue.setSize(recordValue.getLength) // length cannot be long type. If it required a long value, re-write 'BytesWritable'

    }
    true
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file FitsRecordReaderSrcDetLev.scala
//=============================================================================