/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/May/2024
 * Time:  14h:12m
 * Description: None
 */
package com.sparkFits.sparkProcess.hadoop.sourceDetectionLevel
//=============================================================================
import com.common.configuration.MyConf
import com.common.hadoop.fits.record.FitsRecordReader
import com.common.hadoop.inputFormat.binary.BinaryFileInputFormat
//=============================================================================
import org.apache.hadoop.mapreduce.{InputSplit, TaskAttemptContext}
import scala.util.{Try,Success,Failure}
//=============================================================================
//=============================================================================
object BinaryFileFitsSrcDetLev {
  //---------------------------------------------------------------------------
  private final val RECORD_BYTE_LENGTH = com.common.fits.standard.ItemSize.RECORD_CHAR_SIZE
  private final val recordByteLength =  RECORD_BYTE_LENGTH * getRecordMultiplier
  //---------------------------------------------------------------------------
  private def getRecordMultiplier() = {
    var multiplier = 0
    Try {
      multiplier = MyConf(MyConf.c.getString("Spark.conf")).getInt("Hadoop.recordMultiplier")
    }
    match {
      case Success(_) =>
        multiplier //running SPARK in local
      case Failure(_) =>
        1000 * 1 //default value: running SPARK in remote
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import BinaryFileFitsSrcDetLev._
class BinaryFileFitsSrcDetLev() extends BinaryFileInputFormat {
  //---------------------------------------------------------------------------
  val recordByteLength = BinaryFileFitsSrcDetLev.recordByteLength
  //---------------------------------------------------------------------------
  override def createRecordReader(inputSplit: InputSplit
                                  , context: TaskAttemptContext): FitsRecordReader = {
    initRecordReader(context)
    if (recordByteLength <= 0) {
      error("BinaryFileFITS inputFormat " + recordByteLength + " is invalid.  It should be set to a value greater than zero")
      null
    }
    else new FitsRecordReaderSrcDetLev(recordByteLength) //override the RecordReader class
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file BinaryFileFitsSrcDetLev.scala
//=============================================================================