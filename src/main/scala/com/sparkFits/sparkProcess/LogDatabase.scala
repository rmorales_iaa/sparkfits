/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  08/May/2024
 * Time:  13h:01m
 * Description: None
 */
package com.sparkFits.sparkProcess

//=============================================================================
import com.common.database.mongoDB.MongoDB
import org.mongodb.scala.{Document, MongoClient, MongoCollection, MongoDatabase}
//=============================================================================
//=============================================================================
object LogDatabase {
  //---------------------------------------------------------------------------
  //database is created in the workers, so provide a default configuration
  private final val DEFAULT_DATABASE_NAME = "spark_app_log"
  private final val URI                   = "mongodb://rafa:rafaMongoIAA@nakbe:27017/"
  private final val COMPRESSOR            = "zlib"
  private final val IDLE_TIME_SECONDS     = 20
  //---------------------------------------------------------------------------
  final val COL_NAME_SOURCE_COUNT = "source_count"
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.sparkFits.sparkProcess.LogDatabase._
//=============================================================================
case class LogDatabase(databaseName:String = LogDatabase.DEFAULT_DATABASE_NAME
                          , collectionName:String = "entry_seq") extends MongoDB {
  //---------------------------------------------------------------------------
  private val r: (MongoClient, MongoDatabase, MongoCollection[Document]) =
                  init(false
                       , Some(URI)
                       , Some(COMPRESSOR)
                       , Some(IDLE_TIME_SECONDS)
                  )
  val client: MongoClient = r._1
  val database: MongoDatabase = r._2
  val collection: MongoCollection[Document] = r._3
  connected = true
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file LogDatabase.scala
//=============================================================================