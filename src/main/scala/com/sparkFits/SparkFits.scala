/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.sparkFits
//=============================================================================
//=============================================================================
import com.common.configuration.MyConf
import com.common.util.time.Time
import com.sparkFits.commandLine.CommandLineParser
import com.common.logger.MyLogger
import com.common.spark.MySpark
import com.common.spark.dataFrame.MyDataframe
import com.common.util.string.MyString
import com.common.util.time.Time._
import com.sparkFits.sparkProcess.{ProcessFITS, CalculateSourceDetectionLevel}
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import org.slf4j.LoggerFactory
import java.time.Instant
//=============================================================================
//=============================================================================
object SparkFits {
  //---------------------------------------------------------------------------
  val COMMAND_INFO_LEFT_SIDE_SIZE = 28
  //---------------------------------------------------------------------------
  //Spark
  private var spark: MySpark = _
  //---------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------
import SparkFits._
case class SparkFits() extends App with MyLogger {
  //---------------------------------------------------------------------------
  private var lastCommand = ""
  //---------------------------------------------------------------------------
  private def reduceLoggerVerbose() = {

    val loggerContext = LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext]

    //disable 'reactor' logging up to level error
    loggerContext.getLogger("reactor.util").setLevel(Level.WARN)

    //disable all libraries that starts with org (like spark) to logging up to level error
    loggerContext.getLogger("org.sparkproject").setLevel(Level.WARN)
    loggerContext.getLogger("org.apache.spark").setLevel(Level.WARN)
    loggerContext.getLogger("org.apache.hadoop").setLevel(Level.WARN)
    loggerContext.getLogger("org.mongodb.driver").setLevel(Level.WARN)
    loggerContext.getLogger("io.netty").setLevel(Level.WARN)
  }
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {
    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    if (spark == null) {
      val sparkConfFileName = MyConf.c.getString("Spark.conf")
      val sparkConf = MyConf(sparkConfFileName)

      //only use in IntellJ idea
      //if using spark-submit form console then disable this function
      reduceLoggerVerbose()
      spark = MySpark("SparkFits",sparkConf)

      //set the jar used to be distributed across the cluster
      spark.sparkContext.addJar("deploy/sparkFits.jar")

      setGlobalObjects()
    }
  }
  //---------------------------------------------------------------------------
  private def finalActions(startMs: Long): Unit = {
    if (spark != null) spark.close()
    warning(s"SparkFits elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}")
  }
  //-------------------------------------------------------------------------
  private def setGlobalObjects(): Unit = {
    if (spark != null && spark.getUseSpark) {
      MyDataframe.sqlContext = spark.getSqlContext
    }
  }
  //---------------------------------------------------------------------------
  private def commandVersion(): Boolean = {
    lastCommand = CommandLineParser.COMMAND_VERSION
    println(Version.value)
    true
  }
  //---------------------------------------------------------------------------
  private def commandSourceCount(cl: CommandLineParser): Boolean = {
    lastCommand = cl.srcCount()
    val inputPath = cl.srcCount()
    val subWidth  = cl.subWidth()
    val subHeight  = cl.subHeight()
    val sigmaMultiplier  = cl.sigmaMultiplier()
    val sourceDetectionLevel  = cl.sourceDetectionLevel()
    val limitingDataSetItemCount = cl.limitingDataSetItemCount.isSupplied
    val memoryFlag = cl.memory.isSupplied
    val memoryNsaFlag = cl.memoryNsa.isSupplied
    val srcMinPix = cl.srcMinPix()
    val srcMaxPix = cl.srcMaxPix()

    info("Program arguments")
    info(s"${MyString.rightPadding("\tinput path", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputPath'")
    info(s"${MyString.rightPadding("\tsub-image width", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$subWidth' pix")
    info(s"${MyString.rightPadding("\tsub-image height", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$subHeight' pix")
    info(s"${MyString.rightPadding("\tsigma multiplier", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$sigmaMultiplier'")
    if (limitingDataSetItemCount)
      info(s"${MyString.rightPadding("\tlimiting the data set item count to", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${cl.limitingDataSetItemCount()}'")

    if (sourceDetectionLevel != -1)
      info(s"${MyString.rightPadding("\tsource detection level", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$sourceDetectionLevel'")

    if (cl.memory.isSupplied)
      info(s"${MyString.rightPadding("\tprocessing data in memory with sequential storage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$memoryFlag'")
    else if (cl.memoryNsa.isSupplied)
      info(s"${MyString.rightPadding("\tprocessing data in memory with NON sequential storage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$memoryNsaFlag'")
    else
      info(s"${MyString.rightPadding("\tprocessing data using Hadoop's data partition", COMMAND_INFO_LEFT_SIDE_SIZE)}'")

    info(s"${MyString.rightPadding("\tsource min pix count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$srcMinPix'")
    info(s"${MyString.rightPadding("\tsource max pix count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$srcMaxPix'")

    if (inputPath.isEmpty) return error(s"Input data is empty'$inputPath'")

    ProcessFITS.run(spark
                     , inputPath
                     , subWidth
                     , subHeight
                     , sigmaMultiplier
                     , sourceDetectionLevel
                     , sourceRestriction = Some((srcMinPix,srcMaxPix))
                     , if (limitingDataSetItemCount) Some(cl.limitingDataSetItemCount()) else None
                     , memoryFlag
                     , memoryNsaFlag
                   )
    true
  } //---------------------------------------------------------------------------

  private def commandSourceDetectionLevel(cl: CommandLineParser): Boolean = {
    lastCommand = cl.srcDetectLevel()
    val inputPath = cl.srcDetectLevel()
    val subWidth = cl.subWidth()
    val subHeight = cl.subHeight()
    val sigmaMultiplier  = cl.sigmaMultiplier()
    val memoryFlag = cl.memory.isSupplied
    val memoryNsaFlag = cl.memoryNsa.isSupplied

    info("Program arguments")
    info(s"${MyString.rightPadding("\tinput path", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputPath'")
    info(s"${MyString.rightPadding("\tsub-image width", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$subWidth'")
    info(s"${MyString.rightPadding("\tsub-image height", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$subHeight'")
    info(s"${MyString.rightPadding("\tsigma multiplier", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$sigmaMultiplier'")

    if (cl.memory.isSupplied)
      info(s"${MyString.rightPadding("\tprocessing data in memory with sequential storage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$memoryFlag'")
    else if (cl.memoryNsa.isSupplied)
      info(s"${MyString.rightPadding("\tprocessing data in memory with NON sequential storage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$memoryNsaFlag'")
    else
      info(s"${MyString.rightPadding("\tprocessing data using Hadoop's data partition", COMMAND_INFO_LEFT_SIDE_SIZE)}'")

    if (inputPath.isEmpty) return error(s"Input data is empty'$inputPath'")

    val verbose = true
    CalculateSourceDetectionLevel.run(spark
                                      , inputPath
                                      , subWidth
                                      , subHeight
                                      , sigmaMultiplier
                                      , memoryFlag
                                      , memoryNsaFlag
                                      , verbose)
    true
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser): Boolean = {
    if (cl.version.isSupplied) return commandVersion()
    if (cl.srcCount.isSupplied) return commandSourceCount(cl)
    if (cl.srcDetectLevel.isSupplied) return commandSourceDetectionLevel(cl)
    error("Unknown command")
    false
  }
  //---------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {
    val startMs = System.currentTimeMillis

    initialActions()

    commandManager(cl)
  //  SandBox.run()

    finalActions(startMs)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SparkFits.scala
//=============================================================================
