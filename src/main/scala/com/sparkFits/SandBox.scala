/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  30/Sep/2023
 * Time:  19h:09m
 * Description: None
 */
package com.sparkFits
//=============================================================================
import com.common.csv.CsvRead
import com.common.fits.standard.Keyword
import com.common.fits.standard.block.Block
import com.common.fits.standard.fits.{Fits, FitsLoad}
import com.common.fits.standard.structure.hdu.HDU
import com.common.geometry.point.Point2D
import com.common.geometry.segment.segment2D.Segment2D
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.NSA_Image
import com.common.image.nsaImage.source.{Source, SourceSynthesization}
import com.common.logger.MyLogger
import com.common.stream.local.{LocalInputStream, LocalOutputStream}
import com.common.util.pattern.Pattern.parsePattern
import com.sparkFits.sparkProcess.CountSourcesInFile

import java.awt.Font
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object SandBox extends MyLogger {

  //---------------------------------------------------------------------------
  def extendImageData(inputFileName: String
                      , outputFileName: String
                      , pixBlockRepetition: Int): Unit = {

    //load input file and calculate the the data positions
    val img = FitsLoad.load(inputFileName, readDataBlockFlag = false, ignoreDisconf = true).get
    val primaryHDU = img.getPrimaryHdu()
    val header = primaryHDU.recordMap
    val headerByteSize = Block.getBlockByteSize(primaryHDU.headerBlockSize)
    val axisByteSize = header.getAxisTotalByteSize()

    //read the data bytes
    val inputFile = LocalInputStream(inputFileName)
    inputFile.skip(headerByteSize.toInt)
    val inputByteData = inputFile.read(axisByteSize.toInt)._2
    inputFile.close()

    //create output file
    val outputFile = LocalOutputStream(outputFileName)

    //update header
    val axisSeq = header.getAxisSeq()

    header.updateValue(Keyword.KEYWORD_NAXIS + "1", axisSeq.head)
    header.updateValue(Keyword.KEYWORD_NAXIS + "2", axisSeq.last * pixBlockRepetition)

    //save header
    HDU.saveRecordMapAsHeader(outputFile, header, addPadding = true, skipEmptyRecord = true)

    //save data
    for(_ <- 0 until pixBlockRepetition)
       outputFile.write(inputByteData, closeIt = false)

    //save padding
    val paddingByteSize = Block.getLastBlockPaddingByteSize(axisByteSize * pixBlockRepetition)
    if (paddingByteSize > 0) {
      val paddingByte = Array.fill[Byte](paddingByteSize.toInt)(0.toByte)

      outputFile.write(paddingByte, closeIt = true)
    }
    else outputFile.close
  }

  //---------------------------------------------------------------------------
  def sourceDetectionNSA(inputFileName: String
                         , noiseTide: Double
                         , sourceSizeRestriction: (Int, Int)
                         , partitionSubImageSize: Point2D
                         , verbose: Boolean): Unit = {

    val img = NSA_Image.localFileLoad(inputFileName).get
    val sourceSeq = img.getSourceSeq(providedGlobalEstDetectionLevel = Some(noiseTide)
                                     , img.getDimensions
                                     , Some(sourceSizeRestriction)
                                     , Some(partitionSubImageSize)
                                     , verbose)
    warning(s"Found ${sourceSeq.length} sources with nsa")

    Source.saveAsCsv("output/nsa_image.csv", sourceSeq)

    SourceSynthesization.run("output/nsa_image.png"
                             , sourceSeq
                             , img.getX_PixCount
                             , img.getY_PixCount
                             , blackAndWhite = true
                             , font = None
                             )
  }

  //---------------------------------------------------------------------------
  def sourceDetectionAxisPartition(inputFileName: String
                                   , noiseTide: Double
                                   , sourceSizeRestriction: (Int,Int)
                                   , partitionSubImageSize:Point2D): Unit = {


    val providedGlobalEstDetectionLevel = Some(noiseTide)


    val img = MyImage(inputFileName)
    val sourceSeq = img.findSourceByAxisPartition(providedGlobalEstDetectionLevel.get
                                                  , sourceSizeRestriction = Some(sourceSizeRestriction)
                                                  , partitionWidth = partitionSubImageSize.x
                                                  , partitionHeight = partitionSubImageSize.y)


    warning(s"Found ${sourceSeq.length} sources with axis partition")

    Segment2D.saveAsCsv(sourceSeq ,"output/axis_partition_image.csv")

    MyImage.synthesizeImage("output/axis_partition_image.png"
      , sourceSeq
      , img.getDimension()
      , font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10))
    //  , blackAndWhite = true
    )
  }


  //---------------------------------------------------------------------------
  def missingImages() = {
    //obtained with henosis mpo stats
    val csvFileName = "/home/rafa/Downloads/deleteme/henosis/2024-05-31T14:50:22:284+02/image_with_no_mpo.csv"
    val csv = CsvRead(csvFileName, hasHeader = false)
    csv.read()
    //-------------------------------------------------------------------------
    val invalidPatternSeq = List("land\\d{3}".r
                                , ".*-occ.*".r)
    val collectdMpoNameSeq = ArrayBuffer[String]()
    //-------------------------------------------------------------------------
    def passFilter(s: String): Boolean = {

      val isGood = !parsePattern(s, invalidPatternSeq)

      if (isGood) {
        val fits = MyImage(s).getSimpleFits()
        val objectName = fits.getStringValueOrEmptyNoQuotation("OM_OBJEC").toLowerCase().trim
        if (objectName == "none") return false
        else
          collectdMpoNameSeq += objectName
      }
      isGood
    }
    //-------------------------------------------------------------------------
    val p = csv.getCol("col_0000")

    val filteredP = p.filter(s=> passFilter(s.split(",").head))


    collectdMpoNameSeq.foreach(println(_))
    //filteredP .foreach(println(_))

    println()
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def compareCsv() = {
    val csvNameA = "/home/rafa/proyecto/sparkFits/output/nsa_image.csv"
    //val csvNameB = "/home/rafa/proyecto/sparkFits/output/axis_partition_image.csv"
    val csvNameB = "/home/rafa/proyecto/m2/output/classic_detection.csv"

    val csvReadA = CsvRead(csvNameA)
    val csvReadB = CsvRead(csvNameB)
    csvReadA.read()
    csvReadB.read()


    csvReadA.compareDoubleCol(csvReadB,"centroid_x", limitResult = Some(15))
  }
  //---------------------------------------------------------------------------
  def run() =  {

    //compareCsv()
   //------------------------------------

    //val f = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    //val noiseTide = 9390.203154

    val f = "/home/rafa/Downloads/deleteme/small_set/javalambre.fits"
    val noiseTide = 1502
    val sourceSizeRestriction = (8, 3000)
    val providedSubImageSize = Point2D(256, 256)

    //sourceDetectionNSA(f, noiseTide, sourceSizeRestriction, providedSubImageSize)
    //println("---------------------")
    sourceDetectionAxisPartition(f, noiseTide, sourceSizeRestriction, providedSubImageSize)

    //------------------------------------


    // missingImages()
    /*
    extendImageData("/home/rafa/Downloads/deleteme/small_set/javalambre.fits"
                  , "/home/rafa/Downloads/deleteme/small_set/javalambre_extended_6GB.fits"
                  , pixBlockRepetition = 20)
     */

    //geneerateLargePng()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================

//=============================================================================
//End of file SandBox.scala
//=============================================================================