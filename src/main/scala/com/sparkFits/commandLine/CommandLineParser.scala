/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  19/Jun/2023
 * Time:  20h:40m
 * Description: None
 */
package com.sparkFits.commandLine
//=============================================================================
import com.common.logger.MyLogger
import com.sparkFits.Version
import org.rogach.scallop._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_VERSION                    = "version"
  final val COMMAND_SOURCE_COUNT               = "src-count"
  final val COMMAND_SOURCE_DETECTION_LEVEL     = "src-detect-level"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_VERSION
    , COMMAND_SOURCE_COUNT
    , COMMAND_SOURCE_DETECTION_LEVEL
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) with MyLogger {
  version(Version.value + "\n")
  banner(s"""sparkFits syntax
            |sparkFits [configurationFile][command][parameters]
            |[commnads and parameters]
            |\t[$COMMAND_VERSION]
            |\t[$COMMAND_SOURCE_COUNT]
            |\t[$COMMAND_SOURCE_DETECTION_LEVEL]
            | #---------------------------------------------------------------------------------------------------------
            |Example 0: Show version
            |  java -jar sparkFits.jar --$COMMAND_VERSION
            | #---------------------------------------------------------------------------------------------------------
            |Example 1: Process all FITS files of directory using SPARK to detect the sum of all sources detected in all images.
            | To estimate the source detection level, each image will be divided in a mesh of 32x64 sub-images and the
            | median of background and median of backgroundRMS of all sub-images will calculated. The final value is:
            |             sourceDetectionLevel = background + (backgroundRMS * sigmaMultiplier)
            | The input must be stored in hadoop file system.
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_COUNT d --sub-width 32  --sub-height 64 --sigma-multiplier 2.5
            |
            |#---------------------------------------------------------------------------------------------------------
            |Example 1.a: Same as 1m but limiting the amount of items in the data set to be processed to 100
            | The input must be stored in hadoop file system.
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_COUNT d --sub-width 32  --sub-height 64 --sigma-multiplier 2.5 --limiting-data-set-item-count 100
            |
            | #---------------------------------------------------------------------------------------------------------
            |Example 2: Process a single FITS file 'f' using SPARK to detect the source detection level of the image.
            | The image will be divided in a mesh of 32x64 sub-images and the median of background and median of
            | backgroundRMS of all sub-images will calculated. The final value is:
            |             sourceDetectionLevel = background + (backgroundRMS * sigmaMultiplier)
            | The input must be stored in hadoop file system.
            | The data processing is carried out in memory using sequential arrays, not using Hadoop's storage partition
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_DETECTION_LEVEL f --sub-width 32  --sub-height 64 --sigma-multiplier 2.5 --memory
            |
            |#---------------------------------------------------------------------------------------------------------
            |Example 2.a: Same as 2, but the data processing is carried out in memory with non sequential storage. 
            |Not using Hadoop's storage partition
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_DETECTION_LEVEL f --sub-width 32  --sub-height 64 --sigma-multiplier 2.5 --memory-nsa
            |                        
            | #---------------------------------------------------------------------------------------------------------
            |Example 2.b: Same as 2, but the data processing is carried out using Hadoop's storage partition
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_DETECTION_LEVEL f --sub-width 32  --sub-height 64 --sigma-multiplier 2.5
            |
            | #---------------------------------------------------------------------------------------------------------
            |Example 3: Process a single FITS file 'f' using SPARK to count the detected sources using the source
            |detection level 300 (previously calculated with command $COMMAND_SOURCE_DETECTION_LEVEL)
            |The image will be divided in a mesh of 32x64 sub-images.
            |The input must be stored in hadoop file system.
            | The data processing is carried out in memory using sequential arrays, not using Hadoop's storage partition
            |The source pix count must be greater or equal than 8 and less or equal than 3000
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_COUNT f --sub-width 32  --sub-height 64 --source-detection-level 300 --src-min-pix 8 --src-max-pix 3000 --memory
            |
            |#---------------------------------------------------------------------------------------------------------
            |Example 3.b: Same as 3, but the data processing is carried out in memory with non sequential storage. 
            |Not using Hadoop's storage partition
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_COUNT f --sub-width 32  --sub-height 64 --source-detection-level 300 --src-min-pix 8 --src-max-pix 3000 --memory-nsa
            |  
            |#---------------------------------------------------------------------------------------------------------
            |Example 3.c: Same as 3, but the data processing is carried out using Hadoop's storage partition
            |
            |  java -jar sparkFITS.jar --$COMMAND_SOURCE_COUNT f --sub-width 32  --sub-height 64 --source-detection-level 300 --src-min-pix 8 --src-max-pix 3000
            | #---------------------------------------------------------------------------------------------------------
            |Options:
            |""".stripMargin)
  footer("\nFor detailed information, please consult the documentation!")
  //---------------------------------------------------------------------------
  val version = opt[Boolean](
      required = false
    , noshort = true
    , descr = "Program version\n"
  )
  //---------------------------------------------------------------------------
  val srcCount = opt[String](
    required = false
    , noshort = true
    , descr = "Count the detected sources in a file or in all files of a directory\n"
  )
  //---------------------------------------------------------------------------
  val srcDetectLevel = opt[String](
    required = false
    , noshort = true
    , descr = "Min pixel value to consider it as part of a source\n"
  )
  //---------------------------------------------------------------------------
  val subWidth = opt[Int](
    required = false
    , noshort = true
    , default = Some(64)
    , descr = "Sub-image width in pixels\n"
  )
  //---------------------------------------------------------------------------
  val subHeight = opt[Int](
    required = false
    , noshort = true
    , default = Some(64)
    , descr = "Sub-image height in pixels\n"
  )
  //---------------------------------------------------------------------------
  val sigmaMultiplier = opt[Double](
    required = false
    , noshort = true
    , default = Some(2.5)
    , descr = "multiplier used in the calculation of the source detection level\n"
  )
  //---------------------------------------------------------------------------
  val sourceDetectionLevel = opt[Double](
    required = false
    , noshort = true
    , default = Some(-1)
    , descr = "Image source detection level in pixel value\n"
  )
  //---------------------------------------------------------------------------
  val limitingDataSetItemCount = opt[Int](
    required = false
    , noshort = true
    , default = None
    , descr = "Limit the amount of items in the data set to be processed\n"
  )
  //---------------------------------------------------------------------------
  val memory = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(false)
    , descr = "The data processing is carried out in memory not using Hadoop's storage partition\n"
  )
  //---------------------------------------------------------------------------
  val memoryNsa = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(false)
    , descr = "The data processing is carried out in memory with non sequential storage. Not using Hadoop's storage partition\n"
  )
  //---------------------------------------------------------------------------
  val srcMinPix = opt[Int](
    required = false
    , noshort = true
    , default = Some(8)
    , descr = "Limit the amount of items in the data set to be processed\n"
  )
  //---------------------------------------------------------------------------
  val srcMaxPix = opt[Int](
    required = false
    , noshort = true
    , default = Some(3000)
    , descr = "Limit the amount of items in the data set to be processed\n"
  )
  //---------------------------------------------------------------------------
  val commandSeq = Seq(version, srcCount, srcDetectLevel)
  mutuallyExclusive(commandSeq:_*)
  requireOne       (commandSeq:_*)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================
