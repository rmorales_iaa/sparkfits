package com.nsa

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  21/May/2024 
 * Time:  13h:30m
 * Description: None
 */
//=============================================================================
//=============================================================================
import com.UnitSpec
import com.common.configuration.MyConf
import com.common.geometry.point.Point2D
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.NSA_Image
//=============================================================================
//=============================================================================
class NSA_Image_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  def maxX = 3192 - 1 //x axis pix count - 1
  def maxY = 2128 - 1 //y axis pix count - 1
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  /*
  //---------------------------------------------------------------------------
  "an image" must "be find its best data type" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val nsaFITS = NSA_Image.load(imageName).get

    val classicPixelSeq = MyImage(imageName).data
    val nsaPixelSeq = nsaFITS.collect.map{ b=> b.asInstanceOf[Int]}
    assert((nsaPixelSeq zip classicPixelSeq) .forall{p=> p._1 == p._2.toInt})
  }

  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE pixel first" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(0, 0)
    val max = Point2D(0, 0)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }
  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE pixel last" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(maxX, maxY)
    val max = Point2D(maxX, maxY)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }

  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE Col first" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(0, 0)
    val max = Point2D(0, maxY)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }
  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE 1 Col last" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(maxX, 0)
    val max = Point2D(maxX, maxY)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }

  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE row first" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(0, 0)
    val max = Point2D(maxX, 0)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }
  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE 1 row last" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(0, maxY)
    val max = Point2D(maxX, maxY)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }
  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE 3" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(0, 0)
    val max = Point2D(maxX, maxY)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }
  //---------------------------------------------------------------------------
  "an image" must "be loaded and manages as nsa array. CASE 4" in {

    val imageName = "/home/rafa/Downloads/deleteme/single_file/a.fits"
    val img = MyImage(imageName)
    val min = Point2D(147, 154)
    val max = Point2D(1477, 1547)
    val classicPixelSeq = img.getSubMatrix(min, max).data

    val nsaImage = NSA_Image.load(imageName).get
    val nsaResult = nsaImage
      .getSubImage(min, max)
      .get

    val nsaPixelSeq = nsaResult.collect.map { b => b.asInstanceOf[Int] }

    assert(classicPixelSeq.length == nsaPixelSeq.length)
    assert((nsaPixelSeq zip classicPixelSeq).forall { p => p._1 == p._2.toInt })
  }*/
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Image_Spec
//=============================================================================