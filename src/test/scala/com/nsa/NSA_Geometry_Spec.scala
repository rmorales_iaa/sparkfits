package com.nsa

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  21/May/2024 
 * Time:  13h:30m
 * Description: None
 */
//=============================================================================
//=============================================================================
import com.UnitSpec
import com.common.image.myImage.MyImage
import com.common.image.nsaImage.geometry.asterism.MatchAsterism
//=============================================================================
import com.common.image.nsaImage.geometry.point.Point2D
import com.common.image.nsaImage.geometry.polygon.Polygon
//=============================================================================
//=============================================================================
class NSA_Geometry_Spec extends UnitSpec {
  /*
  //---------------------------------------------------------------------------
 "an polygon" must "be calculate its invariants" in {

   val vertexSeqA = Array (
                       (0d,0d)
                     , (1d,0d)
                     , (1.5d,1d)
                     , (0.5d,1.5d)
                     , (-0.5d,1.0d)
                   )

   val vertexSeqB = (vertexSeqA map (p=> (p._1 * 11.2 ,p._2 * 11.2))).reverse


   val polyA = Polygon.buildWithDouble(vertexSeqA)
   polyA.saveAsHtml("output/polA.html")
   val invariantA = polyA.propeties.itgt
   val A = polyA.sidesDoNotCross

   val polyB = Polygon.buildWithDouble(vertexSeqB)
   val invariantB = polyB.propeties.itgt
   val B = polyB.sidesDoNotCross

   assert(invariantA.isEqualTo(invariantB, minDifftoBeEqual = 10e-5))
 } */

  /*
  //---------------------------------------------------------------------------
  "two set of points" must "must be matched" in {

    val catalogPointSeq = Array(
        Point2D[Double](0d, 0d)
      , Point2D[Double](1d, 0d)
      , Point2D[Double](1.5d, 1d)
      , Point2D[Double](0.5d, 1.5d)
      , Point2D[Double](-0.5d, 1.0d)
    )

    val samplegPointSeq =  (catalogPointSeq map ( _ * 11.2)).reverse

    val r =  matchAsterism(catalogPointSeq
                           , samplegPointSeq
                           , _maxAsterismPolygonOrder = catalogPointSeq.length)

     assert(true)
  }
*/
  //---------------------------------------------------------------------------
  "two set of images" must "must be registered" in {
    val imageCatalog = MyImage("/home/rafa/Downloads/deleteme/small_Set_3/CATALOG_science_2020_09_17T04_24_25_350_BOSQUE_ALEGRE_768x512_JOHNSON_R_20s_aletheia-842_r_apogee_usb_net_aletheia.fits")
    val imageSample = MyImage("/home/rafa/Downloads/deleteme/small_Set_3/SAMPLE_science_2020_09_17T05_34_47_370_BOSQUE_ALEGRE_768x512_JOHNSON_R_20s_aletheia-1041_r_apogee_usb_net_aletheia.fits")

    val MAX_SOURCE_COUNT_TO_CONSIDER  = 20
    val PERCENTAGE_OF_SOURCES_TO_MATCH  = 80

    val sourceCatalogCentroidSeq = imageCatalog
      .findSourceSeq(noiseTide = 167.6, sourceSizeRestriction = Some((3,3000)))
      .toSegment2D_seq()
      .sortWith(_.getFlux() > _.getFlux())
      .take(MAX_SOURCE_COUNT_TO_CONSIDER)
      .map(_.getCentroid())

    val sampleCatalogCentroidSeq = imageSample
      .findSourceSeq(noiseTide = 176.1, sourceSizeRestriction = Some((3,3000)))
      .toSegment2D_seq()
      .sortWith(_.getFlux() > _.getFlux())
      .take(MAX_SOURCE_COUNT_TO_CONSIDER)
      .map(_.getCentroid())
/*
    val maxAsterismPolygonOrder = Math.round(Math.min(sourceCatalogCentroidSeq.length
                                                        ,sampleCatalogCentroidSeq.length) * PERCENTAGE_OF_SOURCES_TO_MATCH * 0.01)

    val matchAsterism = MatchAsterism(sourceCatalogCentroidSeq map (p => Point2D[Double](p.x, p.y))
                                      , sampleCatalogCentroidSeq map (p => Point2D[Double](p.x, p.y))
                                      , maxAsterismPolygonOrder.toInt)


    val aft = matchAsterism.calculate()
    if (aft._1.isDefined) {
      MyImage(imageSample.applyAffineTransformation(aft._1.get
                                            , newName = "output/deleteme.fits"
                                            , backgroundValue  = 100
                                            , integralInterpolationType = 1)
               , imageSample.getSimpleFits()).saveAsFits("output/deleteme.fits")
    }
*/
    assert(true)
  }
//---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Geometry_Spec
//=============================================================================