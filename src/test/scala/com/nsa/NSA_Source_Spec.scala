package com.nsa

/**
 * Created by: Rafael Morales (rmorales@iaa.es) 
 * Date:  21/May/2024 
 * Time:  13h:30m
 * Description: None
 */
//=============================================================================
//=============================================================================
import com.UnitSpec
import com.common.configuration.MyConf
import com.common.dataType.array.{NSA_Int}
import com.common.geometry.point.Point2D
import com.common.image.nsaImage.source.{Source, SourceDetection}
import com.common.image.nsaImage.{ NSA_SubImage}
//=============================================================================
//=============================================================================
class NSA_Source_Spec extends UnitSpec {
  def maxX = 3192 - 1 //x axis pix count - 1
  def maxY = 2128 - 1 //y axis pix count - 1
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  val blockByteSize = 2880
  type T = Int
  //---------------------------------------------------------------------------
  def buidSquaredSubImage(data: Array[Array[T]]
                           , offset: Point2D) = {

    val nsa = NSA_Int(data.reverse, blockByteSize)

    val colCount = nsa.dataBlockSeq.head.getItemCount.toInt
    val rowCount = nsa.dataBlockSeq.length

    NSA_SubImage(nsa
      , minPos = offset
      , maxPos = offset + Point2D(colCount, rowCount) - Point2D.POINT_ONE)
  }
  //---------------------------------------------------------------------------
  def buildSource(data: Array[Array[T]]
                  , offset: Point2D
                 , noiseTide: Double = 0
                 , verbose:Boolean = false) = {
    val subImage = buidSquaredSubImage(data, offset)

    val r = SourceDetection[Int](
      noiseTide
      , sourceSizeRestriction = None)
      .findSourceSeq(subImage)
      .head

   if (verbose)  r.prettyPrint()
    r
  }

  //---------------------------------------------------------------------------
  def verify(mergedSourceSeq: Array[Source[T]]
            , sourceA: Source[T]
            , sourceB: Source[T]) = {
    val mergedPosWithValues = mergedSourceSeq.head.getAbsolutePosSeqWithValues()

    val vA = sourceA.getAbsolutePosSeqWithValues().forall { case (a, b, c) =>
      mergedPosWithValues.exists(p => (p._1 == a) && (p._2 == b) && (p._3 == c))
    }

    val vB = sourceB.getAbsolutePosSeqWithValues().forall { case (a, b, c) =>
      mergedPosWithValues.exists(p => (p._1 == a) && (p._2 == b) && (p._3 == c))
    }
    vA && vB
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file NSA_Source_Spec
//=============================================================================