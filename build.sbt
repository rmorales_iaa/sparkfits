//-----------------------------------------------------------------------------
// Use the same scala version for running application and spark
// To be sure, download spark source and check the scala version in directory "build/·
lazy val myScalaVersion =  "2.13.14"
lazy val programName    = "sparkFits"
lazy val programVersion = "0.0.1"
lazy val authorList     = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val license        = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := myScalaVersion
  , description        := "Apache Spark for FITS standard"
  , organization       := "IAA-CSIC"
  , homepage           := Some(url("http://www.iaa.csic.es"))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.sparkFits.Main"
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsCsvVersion   = "1.10.0"
val apacheCommonsIO_Version   = "2.16.1"
val apacheCommonsMathVersion  = "3.6.1"
val apacheSparkVersion        = "3.5.2"
val fastParseVersion          = "3.1.1"
val httpVersion               = "2.4.2"
val jamaVersion               = "1.0.3"
val jsonParsingUpickleVersion = "4.0.1"
val jsonParsingJsoniterVersion = "2.30.9"
val javaxMailVersion         = "1.6.2"
val lz4_CompressionVersion   = "1.8.0"
val logApacheApiScalaVersion = "13.1.0"
val logApacheCoreVersion     = "2.23.1"
val logLogbackVersion        = "1.5.8"
val logSlf4jApiVersion       = "2.0.16"
val mongoScalaDriverVersion  = "5.2.0"
val rTree2DVersion           = "0.11.13"
val scallopVersion           = "5.1.0"
val scalaReflectVersion      = myScalaVersion
val scalaTestVersion         = "3.3.0-SNAP4"
val jSofaVersion             = "20210512"
val typeSafeVersion          = "1.4.3"
//-----------------------------------------------------------------------------
//external source code
excludeFilter in (Compile, unmanagedSources) := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath
    p.contains("database/postgreDB") ||
    p.contains("database/simbad") ||
    p.contains("database/vizier") ||
    p.contains("database/mongoDB/database/apass") ||
    p.contains("database/mongoDB/database/astrometry") ||
    p.contains("database/mongoDB/database/carmencita") ||
    p.contains("database/mongoDB/database/ephemeris") ||
    p.contains("database/mongoDB/database/gaia") ||
    p.contains("database/mongoDB/database/lcdb") ||
    p.contains("database/mongoDB/database/mpc") ||
    p.contains("database/mongoDB/database/observatories") ||
    p.contains("database/mongoDB/database/spiceSPK/") ||
    p.contains("database/mongoDB/database/standardStar") ||
    p.contains("database/mongoDB/database/vr") ||
    p.contains("fits/metaData") ||
    p.contains("fits/synthetize") ||
    p.contains("fits/wcs/fit") ||
    p.contains("image/astrometry") ||
    p.contains("image/catalog") ||
    p.contains("image/estimator/flux/") ||
    p.contains("image/estimator/skyPosition/") ||
    p.contains("image/focusType") ||
    p.contains("image/fumo/") ||
    p.contains("image/mpo") ||
    p.contains("image/myImage/dir") ||
    p.contains("image/occultation") ||
    p.contains("image/photometry") ||
    p.contains("image/registration") ||
    p.contains("image/script") ||
    p.contains("image/web") ||
    p.contains("myJava/nom/tam") ||
    p.contains("myJava/wcs") ||
    p.contains("util/computerInfo")

  })}
//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers ++= Resolver.sonatypeOssRepos("public")
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"   //generate an error when warning raises. Enable when stable version was published
)
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {

  case PathList("META-INF", "versions", "11", "module-info.class") => MergeStrategy.last
  case PathList("META-INF", "versions", "9", "module-info.class")  => MergeStrategy.discard
  case PathList("META-INF", "io.netty.versions.properties")        => MergeStrategy.last
  case PathList("google", "protobuf", n) if n endsWith ".proto"    => MergeStrategy.last

  case "META-INF/versions/9/javax/xml/bind/ModuleUtil.class"       => MergeStrategy.last
  case "META-INF/native-image/org.mongodb/bson/native-image.properties" => MergeStrategy.last

  case "arrow-git.properties"                                      => MergeStrategy.last

  case PathList(ps@_*) if (ps.last == "Log4j2Plugins.dat" ||
    ps.last == "module-info.class" ||
    ps.last == "Log.class" ||
    ps.last == "LogConfigurationException.class" ||
    ps.last == "NoOpLog.class" ||
    ps.last == "SimpleLog.class" ||
    ps.last == "LogFactory.class")  => MergeStrategy.last

  case x => MergeStrategy.defaultMergeStrategy(x)
}
//-----------------------------------------------------------------------------
//tests
test / parallelExecution  := false
//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//-----------------------------------------------------------------------------
//output path for JNI C headers. Generate it with sbt javah
javah / target :=  sourceDirectory.value / "../native/header"
//-----------------------------------------------------------------------------
//dependencies list
lazy val dependenceList = Seq(

  //manage configuration files. https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging
  , "ch.qos.logback" % "logback-classic" % logLogbackVersion
  , "org.slf4j" % "slf4j-api" % logSlf4jApiVersion

  , "org.apache.logging.log4j" %% "log4j-api-scala" % logApacheApiScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % logApacheCoreVersion

  //command line parser : https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % scallopVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsvVersion

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3 https://mvnrepository.com/artifact/org.apache.commons/commons-math3c
  , "org.apache.commons" % "commons-math3" % apacheCommonsMathVersion

  //apache commons file utils
  , "commons-io" % "commons-io" % apacheCommonsIO_Version

  //apache commons; CSV management
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsvVersion

  //Spark and hadoop
  , "org.apache.spark" %% "spark-core" % apacheSparkVersion exclude ("org.apache.logging.log4j", "log4j-slf4j2-impl")
  , "org.apache.spark" %% "spark-sql" % apacheSparkVersion exclude ("org.apache.logging.log4j", "log4j-slf4j2-impl")
  , "org.apache.spark" %% "spark-mllib" % apacheSparkVersion exclude ("org.apache.logging.log4j", "log4j-slf4j2-impl")

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion

  //lz4 compression: 	https://github.com/lz4/lz4-java
  , "org.lz4" % "lz4-java"  % lz4_CompressionVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion

  //http: //javastro.github.io/jsofa/
  , "org.javastro" % "jsofa" % jSofaVersion

  //scala wrapper for HttpURLConnection. OAuth included. https://github.com/scalaj/scalaj-http
  , "org.scalaj" %% "scalaj-http" % httpVersion

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //R-tree on 2d: https://github.com/plokhotnyuk/rtree2d
  , "com.github.plokhotnyuk.rtree2d" %% "rtree2d-core" % rTree2DVersion

  //json parsing: https://github.com/plokhotnyuk/jsoniter-scala
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core" % jsonParsingJsoniterVersion
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % jsonParsingJsoniterVersion % "provided"

  //mail agent: https://javaee.github.io/javamail/
  , "com.sun.mail" % "javax.mail" % javaxMailVersion

  //grammar parserTform: https://github.com/com-lihaoyi/fastparse
  , "com.lihaoyi" %% "fastparse" % fastParseVersion

  //json parsing: // https://mvnrepository.com/artifact/com.lihaoyi/upickle
  , "com.lihaoyi" %% "upickle" % jsonParsingUpickleVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'build.sbt'
//-----------------------------------------------------------------------------
